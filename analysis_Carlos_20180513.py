import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import roc_curve
from sklearn.externals import joblib
import statsmodels.formula.api as sm
import random
import itertools
from copy import deepcopy
from collections import OrderedDict

import sys
sys.path.insert(0, './scripts')
from nanolyse import nanolyse
from hmmlearn import hmm

def pd_normalize(df, exclude=None):
    """
    Normalize columns of a pandas dataframe. Optionally, provide list of names of columns to exclude from normalization.

    """
    dfm = df.mean()
    dfsd = df.std()
    numeric_vars = list(dfm.index)
    if exclude is not None:
        _ = [numeric_vars.remove(exc) for exc in exclude]
    for n in numeric_vars:
        df[n] = (df[n] - dfm[n]) / dfsd[n]
    return df

def pd_limit(df, limit, exclude=None):
    """
    Remove points outside given limit for all numerical columns not listed in exclude. Only makes sense on normalized
    data I guess...
    """
    numeric_vars = list(df.mean().index)
    if exclude is not None:
        _ = [numeric_vars.remove(exc) for exc in exclude]
    for n in numeric_vars:
            df = df.loc[df[n].abs() < limit ]
    return df

df = pd.read_pickle('train_data_allFiles_pd.pkl')

# Replace spaces, remove dots in feature names
for colname in list(df):
    if ' ' in colname:
        colname_new = colname.replace(' ', '_')
        colname_new = colname_new.replace('.', '')
        df = df.rename(index=str, columns={colname : colname_new})

df_norm = pd_normalize(df, ['pH'])


# Pairs plots
for nsds in range(1,3):
    df_norm_rmOutliers = pd_limit(df_norm, limit=nsds, exclude=['pH'])
    g = sns.pairplot(df_norm_rmOutliers.drop('pH', axis=1), hue="Sequence")
    plt.savefig('pairs_norm_' + str(nsds) + 'sd.png')

# Select columns used in Florian's analysis
used_vars = ['Ires',
             'Ires_SD2',
             'dIres',
             'dIres_SD',
             'L1_Ires_cumsum',
             'L1_Ires_cumsum_SD',
             'L1_dwelltime',
             'Ires_min',
             'Ires_max',
             'dIres_min',
             'dIres_max']
df_norm_used = df_norm[used_vars]

# PCA
pca_obj = PCA(n_components=3)
pca_obj.fit(df_norm_used)
data_rotated = pca_obj.transform(df_norm_used)

df_rotated = pd.DataFrame(data = {
    'pc1': data_rotated[:,0],
    'pc2': data_rotated[:,1],
    'pc3': data_rotated[:,2],
    'Label': df_norm['Label'],
    'Sequence': df_norm['Sequence']
    })

plot_rotated=sns.pairplot(df_rotated, hue='Sequence')
plot_rotated.savefig('pairs_norm_rotated_3pc.png')

df_rotated_sd2 = pd_limit(pd_normalize(df_rotated), 2)
plot_rotated=sns.pairplot(df_rotated_sd2, hue='Sequence')
plot_rotated.savefig('pairs_norm_rotated_3pc_2sd.png')

# Fit logistic model
df_norm_lm = df_norm.loc[df_norm['Sequence'] != 'BLANK']
Sequence_bin = np.zeros(df_norm_lm.shape[0], dtype=int)
Sequence_bin[df_norm_lm['Sequence'] == 'YGGFL'] = 1
df_norm_lm['Sequence_bin'] = Sequence_bin

nb_folds = 5
fold_size = df_norm_lm.shape[0] // nb_folds
df_folds = range(df_norm_lm.shape[0])
random.shuffle(df_folds)
df_folds = [df_folds[i*fold_size:(i+1)*fold_size] for i in range(nb_folds)]

acc_list = []
recall_list = []
precision_list = []
for f in range(nb_folds):
    train_idx =  deepcopy(df_folds)
    val_idx = train_idx.pop(f)
    train_idx = list(itertools.chain.from_iterable(train_idx))
    lm = sm.logit('Sequence_bin ~ Ires + Ires_SD2', df_norm_lm, subset=train_idx).fit(method='bfgs',maxiter=100)
    lm_pred_pd = lm.predict(df_norm_lm.iloc[val_idx])
    lm_pred = np.zeros(len(val_idx), dtype=int)
    lm_pred[lm_pred_pd>0.5] = 1
    acc = np.sum(lm_pred == df_norm_lm.iloc[val_idx].Sequence_bin) * 1.0 / len(val_idx)
    tp = np.sum(np.logical_and(lm_pred == 1, lm_pred == df_norm_lm.iloc[val_idx].Sequence_bin)) * 1.0
    recall = tp / np.sum(df_norm_lm.iloc[val_idx].Sequence_bin == 1)
    precision = tp / np.sum(lm_pred == 1)
    recall_list.append(recall)
    precision_list.append(precision)
    acc_list.append(acc)

print(sum(acc_list) / len(acc_list))
print(sum(recall_list) / len(recall_list))
print(sum(precision_list) / len(precision_list))

fpr, tpr, _ = roc_curve(df_norm_lm.iloc[val_idx].Sequence_bin, lm_pred_pd)
plt.figure()
plt.plot(fpr, tpr, lw = 2)
plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.0])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC: YGGFL vs YGGFM')

plt.savefig('roc_lm_YGGFLvsYGGFM.png')



# Principal component regression (not better...was still based on the entire set of variables)

df_rotated_lm = df_rotated[df_rotated['Sequence'] != 'BLANK']
Sequence_bin = np.zeros(df_rotated_lm.shape[0], dtype=int)
Sequence_bin[df_rotated_lm['Sequence'] == 'YGGFL'] = 1
df_rotated_lm['Sequence_bin'] = Sequence_bin
acc_list_rotated = []

for f in range(nb_folds):
    train_idx =  deepcopy(df_folds)
    val_idx = train_idx.pop(f)
    train_idx = list(itertools.chain.from_iterable(train_idx))
    lm = sm.logit('Sequence_bin ~ pc1 + pc2', df_rotated_lm, subset=train_idx).fit(method='bfgs',maxiter=100)
    lm_pred_pd = lm.predict(df_rotated_lm.iloc[val_idx])
    lm_pred = np.zeros(len(val_idx), dtype=int)
    lm_pred[lm_pred_pd>0.5] = 1
    acc = np.sum(lm_pred.astype(int) == df_rotated_lm.iloc[val_idx].Sequence_bin) * 1.0 / len(val_idx)
    acc_list_rotated.append(acc)
print(sum(acc_list_rotated) / len(acc_list_rotated))


# HMM

# Determine means and sds
levels_dict = OrderedDict()
L0_Ires_vec = np.concatenate(df.L0_Ires)
levels_dict['L0'] = {'mean': np.mean(L0_Ires_vec), 'sd': np.std(L0_Ires_vec)}
for seq in df.Sequence.unique():
    cur_seq = np.concatenate(df.L1_Ires.loc[df.Sequence == seq])
    levels_dict[seq] = {'mean': np.mean(cur_seq), 'sd': np.std(cur_seq)}
_ = levels_dict.pop('BLANK') # TODO: remove, eventually

# Initialize model
hmm_obj = hmm.GaussianHMM(n_components=len(levels_dict), covariance_type='diag', init_params='tc')
nb_blocklevels = len(levels_dict) - 1
startprobs = [1 - 1e-20 * nb_blocklevels] + [1e-20] * nb_blocklevels
hmm_obj.startprob_ = np.array(startprobs)
hmm_obj.means_ = np.array([levels_dict[level]['mean'] for level in levels_dict]).reshape(-1,1)
hmm_obj.covars_ = np.array([levels_dict[level]['sd'] for level in levels_dict]).reshape(-1,1)

# Retrieve & normalize signal for a mixed sample
fn = 'Enkephalin [Met] 0000/20180404_1M_KCl_15mM_Citricacid_9mM_BTP_pH_42_FraC_G13W_neg100mV_10uM_YGGFM_2500nM_YGGFL_FL_0000.abf'

nl = nanolyse()
nl.loadAxon(fn)
nl.get_levels()
nl.get_events(dwelltime=5e-4, skip=2)
signal_norm = nl.signal
for l0_vec, b, e in zip(nl.L0, nl.L0_start, nl.L1_end):
    l0_mean = np.mean(l0_vec)
    signal_norm[b:e] /= l0_mean

# Fit model
hmm_obj.fit(signal_norm.reshape(-1,1))
joblib.dump(hmm_obj, 'hmm_trained.pkl')

levels_list = list(levels_dict)
print('Ratio {v1} vs {v2} is: {score}'.format(
    v1= levels_list[2],
    v2= levels_list[1],
    score=hmm_obj.transmat_[0, 2] / hmm_obj.transmat_[0, 1]))